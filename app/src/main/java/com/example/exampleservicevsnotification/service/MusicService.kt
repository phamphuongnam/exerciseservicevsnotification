package com.example.exampleservicevsnotification.service

import android.app.Notification
import android.app.PendingIntent
import android.app.Service
import android.content.Intent
import android.os.Build
import android.os.IBinder
import android.util.Log
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.example.exampleservicevsnotification.R
import com.example.exampleservicevsnotification.view.MainActivity.Companion.CHANNEL_ID
import com.example.exampleservicevsnotification.view.MainActivity2

@RequiresApi(Build.VERSION_CODES.O)
class MusicService : Service() {

    override fun onStartCommand(intent: Intent?, flags: Int, startId: Int): Int {
        Log.i("TAG", "onStartCommand")
        return START_STICKY
    }

    override fun onBind(intent: Intent?): IBinder? {
        Log.i("TAG", "onBind")
        return null
    }

    override fun onCreate() {
        super.onCreate()
        if (Build.VERSION.SDK_INT >= 26) {
            val pendingIntent: PendingIntent =
                Intent(this, MainActivity2::class.java).let { notificationIntent ->
                    PendingIntent.getActivity(this, 0, notificationIntent, 0)
                }

            val notification: Notification = NotificationCompat.Builder(this, CHANNEL_ID)
                .setContentTitle(getText(R.string.notification_title))
                .setContentText(getText(R.string.notification_message))
                .setContentIntent(pendingIntent)
//                .setTicker(getText(R.string.ticker_text))
                .setSmallIcon(R.drawable.transparency)
//                .setColor(R.drawable.transparency)
                .build()
//            Thread.sleep(5000)
            startForeground(ONGOING_NOTIFICATION_ID, notification)
//            with(NotificationManagerCompat.from(this)) {
//                notify(ONGOING_NOTIFICATION_ID, notification)
//            }
        }
        Log.i("TAG", "onCreate")
// Notification ID cannot be 0.
//        if (Build.VERSION.SDK_INT >= 26) {
//            val CHANNEL_ID = "my_channel_01"
//            val channel = NotificationChannel(
//                CHANNEL_ID,
//                "Channel human readable title",
//                NotificationManager.IMPORTANCE_DEFAULT
//            )
//            (getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager).createNotificationChannel(
//                channel
//            )
//            val notification = NotificationCompat.Builder(this, CHANNEL_ID)
//                .setContentTitle("")
//                .setContentText("").build()
//            startForeground(1, notification)
//        }
    }

    companion object {
        const val ONGOING_NOTIFICATION_ID = 1
    }
}
