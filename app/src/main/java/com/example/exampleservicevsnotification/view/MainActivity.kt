package com.example.exampleservicevsnotification.view

import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.PendingIntent
import android.content.Context
import android.content.Intent
import android.os.Build
import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import com.example.exampleservicevsnotification.R
import com.example.exampleservicevsnotification.service.MusicService
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        createNotificationChannel()
        buttonService.setOnClickListener {
            val intent = Intent(this, MusicService::class.java)
            startService(intent)
//            val pendingIntent: PendingIntent =
//                Intent(this, MainActivity2::class.java).let { notificationIntent ->
//                    PendingIntent.getActivity(this, 0, notificationIntent, 0)
//                }
//
//            val notification: Notification = NotificationCompat.Builder(this, App.CHANNEL_ID)
//                .setContentTitle(getText(R.string.notification_title))
//                .setContentText(getText(R.string.notification_message))
//                .setContentIntent(pendingIntent)
//                .setTicker(getText(R.string.ticker_text))
//                .setSmallIcon(R.drawable.ic_alo)
//                .build()
//            with(NotificationManagerCompat.from(this)) {
//                notify(1, notification)
//            }
        }
    }
    private fun createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            val name = getString(R.string.channel_name)
            val descriptionText = getString(R.string.channel_description)
            val importance = NotificationManager.IMPORTANCE_DEFAULT
            val channel = NotificationChannel(CHANNEL_ID, name, importance).apply {
                description = descriptionText
            }
            // Register the channel with the system
            val notificationManager: NotificationManager =
                getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
            notificationManager.createNotificationChannel(channel)
        }
    }

    companion object {
        const val CHANNEL_ID = "channel_service_example"
    }
}